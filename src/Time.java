public class Time {
    private double seconds;
    private double minutes;
    private String day;

    public Time(double seconds, double minutes, String day) {
        this.seconds = seconds;
        this.minutes = minutes;
        this.day = day;
    }
    public void display() {
        class LocalInner {
         public void displayTime(){
             System.out.println(seconds+":"+minutes+":"+day);
         }

        }
        LocalInner localInnerObject = new LocalInner();
        localInnerObject.displayTime();
    }

}
